from HTTPClient import NVPair
from env import request, extractAll, extract, valueOrDefault
from java.util.regex import Pattern

class ProjectAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : browse projects'),
            'add' : request(testId + 1, 'HTTP-REQ : add project'),
            'view' : request(testId + 2, 'HTTP-REQ : view project'),
            'edit' : request(testId + 3, 'HTTP-REQ : edit project')
        }
        self.patterns = {
            'project_avatars' : Pattern.compile('(?s)class="project-avatar.*?src="(.*?)"'),
            'add_project_user_avatar' : Pattern.compile('(\/secure\/useravatar.*?)\''),
            'view_project_avatar' : Pattern.compile('(?s)id="project-config-header-avatar".*?src="(.*?)"'),
            'edit_project_avatar' : Pattern.compile('(?s)id="project_avatar_image".*?src="(.*?)"'),
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_lead' : Pattern.compile('(?s)name="lead".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?\>(.*?)\<'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"'),
            'atl_token_dialog' : Pattern.compile('(?s)name="atl_token".*?value="(.*?)"')
        }
        
        
    def browse(self, cached=False):
        req = self.requests['browse']

        page = req.GET('/secure/project/ViewProjects.jspa').text

        if not cached:
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_USkppxta/713/4/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_USkppxta/713/4/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/css/batch.css')
            req.GET('/secure/projectavatar?pid=10010&avatarId=10011&size=small')
            req.GET('/s/en_USkppxta/713/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/713/4/_/images/jira111x30.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/new/icon18-charlie.png')
            for avatar in extractAll(page, self.patterns['project_avatars']):
                req.GET(avatar)
        
        return extract(page, self.patterns['atl_token'])

        
    def add(self, token, project={}, cached=False):
        req = self.requests['add']
        
        dialog = req.GET('/secure/admin/AddProject!default.jspa?atl_token=' + token + '&inline=true&decorator=dialog&_=1329900458230').text

        if not cached:        
            req.GET('/s/en_USkppxta/713/4/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_USkppxta/713/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_USkppxta/713/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
            req.GET(extract(dialog, self.patterns['add_project_user_avatar'], (('&amp;', '&'),)))

        req.POST('/secure/admin/AddProject.jspa',
        (
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('name', project['name']),
            NVPair('key', project['key']),
            NVPair('lead', project['lead']),
            NVPair('atl_token', extract(dialog, self.patterns['atl_token_dialog'])),
            NVPair('permissionScheme', '0'),
            NVPair('assigneeType', '2'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
        return self.view(project['key'], cached)
        
        
    def view(self, projectKey, cached=False):
        req = self.requests['view']
        
        page = req.GET('/plugins/servlet/project-config/' + projectKey + '/summary').text
        
        if not cached:
            req.GET('/s/en_USkppxta/713/4/bb7723f5681082fe7297b5992ea49edc/_/download/contextbatch/css/jira.admin.conf,atl.admin/batch.css')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/713/4/bb7723f5681082fe7297b5992ea49edc/_/download/contextbatch/js/jira.admin.conf,atl.admin/batch.js')
            req.GET('/s/en_USkppxta/713/4/bb7723f5681082fe7297b5992ea49edc/_/download/contextbatch/js/jira.admin.conf,atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_USkppxta/713/4/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-issuetypes.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/icon_blockexpanded.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/713/4/_/images/jira111x30.png')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/genericissue.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-workflows.png')
            req.GET('/images/icons/task.gif')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-screens.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-fields.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-settings.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-versions.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-people.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-components.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-permissions.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon48-notifications.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_USkppxta/713/4/5.0.1-SNAPSHOT/_/download/resources/com.atlassian.jira.jira-project-config-plugin:project-config-global/images/icon16-sprite.png')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/new/icon18-charlie.png')  
            req.GET(extract(page, self.patterns['view_project_avatar']))

        return extract(page, self.patterns['atl_token'])
    

    def edit(self, token, project, cached=False):
        req = self.requests['edit']
        pid = project['id']
        
        dialog = req.GET('/secure/project/EditProject!default.jspa?pid=' + pid + '&returnUrl=ViewProjects.jspa&inline=true&decorator=dialog&_=1329901200660').text

        if not cached:
            req.GET(extract(dialog, self.patterns['edit_project_avatar']))
            req.GET('/s/en_USkppxta/713/4/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
            req.GET('/s/en_USkppxta/713/4/1.0/_/images/icons/wait.gif')

        req.POST('/secure/project/EditProject.jspa',
        (
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('name', valueOrDefault(project, 'name', extract(dialog, self.patterns['edit_name']))),
            NVPair('url', ''),
            NVPair('description', valueOrDefault(project, 'description', extract(dialog, self.patterns['edit_description']))),
            NVPair('atl_token', token),
            NVPair('avatarId', ''),
            NVPair('pid', pid),
            NVPair('returnUrl', 'ViewProjects.jspa'),
            
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))

        return self.browse(cached)
