from env import request, extract
from java.util.regex import Pattern

class SavedFilter:
    
    def __init__(self, testIndex):
        self.requests = {
            'manage' : request(testIndex, 'HTTP-REQ : manage saved filters'),
            'use' : request(testIndex + 1, 'HTTP-REQ : use a saved filter')
        }
        self.patterns = {
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"')                         
        }
        
    def manage(self, cached=False):
        req = self.requests['manage']
        
        page = req.GET('/secure/ManageFilters.jspa').text
        
        if not cached:
            req.GET('/s/en_USkppxta/712/7/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_USkppxta/712/7/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/s/en_USkppxta/712/7/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:manageshared/jira.webresources:manageshared.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:managefilters/jira.webresources:managefilters.js')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/cog-dd.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/star_yellow.gif')
            req.GET('/s/en_USkppxta/712/7/_/images/icons/ico_help.png')
            req.GET('/images/icons/filter_48.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/7/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/7/_/images/jira111x30.png')
         
        return extract(page, self.patterns['atl_token'])           


    def use(self, atl_token, savedFilterId, cached=False):
        req = self.requests['use']
        
        req.GET('/secure/IssueNavigator.jspa?mode=hide&atl_token=' + atl_token + '&requestId=' + str(savedFilterId))
        
        if not cached:
            req.GET('/s/en_USkppxta/712/13/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/css/jira.navigator.simple,atl.general/batch.css')
            req.GET('/s/en_USkppxta/712/13/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_USkppxta/712/13/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_USkppxta/712/13/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_USkppxta/712/13/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/js/jira.navigator.simple,atl.general/batch.js')
            req.GET('/s/en_USkppxta/712/13/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            req.GET('/s/en_USkppxta/712/13/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            req.GET('/s/en_USkppxta/712/13/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            req.GET('/s/en_USkppxta/712/13/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            req.GET('/s/en_USkppxta/712/13/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:searchrequestview-charts/com.atlassian.jira.gadgets:searchrequestview-charts.js')
            req.GET('/s/en_USkppxta/712/13/1/_/download/superbatch/css/batch.css')
            req.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            req.GET('/s/en_USkppxta/712/13/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/view_20.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/tools_20.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/permalink_light_16.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/icon_filtercollapse.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/tab_bg.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/star_yellow.gif')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/mod_header_bg.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/icon_descending.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/ico_activeissue.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/priority_major.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/new/icon18-charlie.png')
            req.GET('/s/en_USkppxta/712/13/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_USkppxta/712/13/_/images/jira111x30.png')
