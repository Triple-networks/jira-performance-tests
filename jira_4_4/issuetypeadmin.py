from HTTPClient import NVPair
from env import request, valueOrEmpty
from env import valueOrDefault, extract
from java.util.regex import Pattern

class IssueTypeAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : manage issue types'), 
            'add' : request(testId + 1, 'HTTP-REQ : add issue type'), 
            'edit' : request(testId + 2, 'HTTP-REQ : edit issue type') 
        }
        self.patterns = {
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?value="(.*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }
        
        
    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/ViewIssueTypes.jspa').text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/button_bg.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/_/images/icons/ico_help.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/tab_bg.png')
            req.GET('/images/icons/bug.gif')
            req.GET('/images/icons/genericissue.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/images/icons/task.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/required.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
                
        return extract(page, self.patterns['atl_token'])

    
    def add(self, token, issueType, cached=False):
        req = self.requests['add']

        req.POST('/secure/admin/AddIssueType.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', issueType['name']),
            NVPair('description', valueOrEmpty(issueType, 'description')),
            NVPair('iconurl', '/images/icons/genericissue.gif'),
            NVPair('Add', 'Add'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
                        
        self.browse(cached)

    def edit(self, token, issueType, cached=False):
        req = self.requests['edit']
        tid = issueType['id']
        
        page = req.GET('/secure/admin/EditIssueType!default.jspa?id=' + tid).text
        
        if not cached:
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.css')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.css')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0.9/_/download/batch/com.atlassian.plugin.atlassian-feedback-plugin:feedback/com.atlassian.plugin.atlassian-feedback-plugin:feedback.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avataror/jira.webresources:avataror.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:jira-admin/jira.webresources:jira-admin.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-soy-plugin:soy-deps/com.atlassian.jira.jira-soy-plugin:soy-deps.js')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:avatarpicker/jira.webresources:avatarpicker.js')
            req.GET('/s/en_US-64k3hp/664/2/4.4.4/_/download/batch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch/com.atlassian.jira.jira-admin-quicknav-plugin:admin-quicksearch.js')
            req.GET('/rest/api/1.0/shortcuts/664/1a83ddc68aaff4b4bd38ebf1e8d8c086/shortcuts.js?context=admin')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-64k3hp/664/2/_/images/jira111x30.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/rest/api/1.0/header-separator?color=#ffffff&bgcolor=#114070')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#3c78b5')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/required.png')
            req.GET('/rest/api/1.0/dropdowns?color=#ffffff&bgcolor=#114070')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/button_bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/images/aui-formbar-button-inactive-bg.png')
            req.GET('/s/en_US-64k3hp/664/2/1/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-64k3hp/664/2/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css?media=print')
            req.GET('/s/en_US-64k3hp/664/2/1/_/download/superbatch/css/images/toolbar/aui-toolbar-button-active-bg.png')

        req.POST('/secure/admin/EditIssueType.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', valueOrDefault(issueType, 'name', extract(page, self.patterns['edit_name']))),
            NVPair('description', valueOrDefault(issueType, 'description', extract(page, self.patterns['edit_description']))),
            NVPair('iconurl', '/images/icons/genericissue.gif'),
            NVPair('id', tid),
            NVPair('Update', 'Update'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
        return self.browse(cached)
