from env import randint, randChoice
from net.grinder.script.Grinder import grinder

from config import Config
from dashboard import Dashboard
from issuelist import IssueList
from issuenav import IssueNavigator
from savedfilter import SavedFilter
from userprofile import UserProfile
from projectlist import ProjectList
from userlist import UserList
from issuecreator import IssueCreator
from reports import Reports
from useradmin import UserAdmin
from projectadmin import ProjectAdmin
from issuetypeadmin import IssueTypeAdmin
from workflowadmin import WorkflowAdmin
from screenadmin import ScreenAdmin
from notificationschemeadmin import NotificationSchemeAdmin
from priorityadmin import PriorityAdmin
from statusadmin import StatusAdmin
from resolutionadmin import ResolutionAdmin
from permissionschemeadmin import PermissionSchemeAdmin
from systeminfoadmin import SystemInfoAdmin
from loggingprofilingadmin import LoggingProfilingAdmin
from mailServerAdmin import MailServerAdmin
from generalconfigadmin import GeneralConfigAdmin
from websudo import WebSudo

l = grinder.logger
config = Config()
userList = UserList(config) 

dashboard = Dashboard(100)
issueNav = IssueNavigator(200)
savedFilter = SavedFilter(300)
userProfile = UserProfile(400)
projectList = ProjectList(500,  config.getProjectFile())
issueList = IssueList(600, config)
issueCreator = IssueCreator(700)
reports = Reports(800)

userAdmin = UserAdmin(900)
projectAdmin = ProjectAdmin(1000)
issueTypeAdmin = IssueTypeAdmin(1100)
workflowAdmin = WorkflowAdmin(1200)
screenAdmin = ScreenAdmin(1300)
notificationSchemeAdmin = NotificationSchemeAdmin(1400)
priorityAdmin = PriorityAdmin(1500)
statusAdmin = StatusAdmin(1600)
resolutionAdmin = ResolutionAdmin(1700)
permissionSchemeAdmin = PermissionSchemeAdmin(1800)
systemInfoAdmin = SystemInfoAdmin(1900)
loggingProfilingAdmin = LoggingProfilingAdmin(2000)
mailServerAdmin = MailServerAdmin(2100)
generalconfigadmin = GeneralConfigAdmin(2200)
websudo = WebSudo(2300)

logLevels = ['DEBUG', 'INFO', 'WARN', 'ERROR']
lowercase = 'abcdefghijklmnopqrstuvwxyz'
uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def goHome():
    log('DASHBOARD NOT LOGGED IN')
    dashboard.notLoggedIn()
    
def doLogin():
    log('LOGIN & DASHBOARD')
    global user
    user = userList.getRandomUser()
    l.output('logging in as ' + user['username'] + ' :: ' + user['password'])
    dashboard.login(user['username'], user['password'])
    dashboard.loggedIn()
    
def doLoginAsAdmin():
    log('LOGIN & DASHBOARD')
    global user
    user = userList.getAdminUser()
    l.output('logging in as ' + user['username'] + ' :: ' + user['password'])
    dashboard.login(user['username'], user['password'])
    dashboard.loggedIn()
    
def browseIssues(count):
    log('VIEW ISSUES')
    issues = issueList.getIssues()

    for i in range(count):
        issue = issues[i]
        cached = i > 0
        l.output('viewing issue ' + issue.getKey() + ' cached=' + str(cached))
        issue.view(cached)
        
def browseIssueTabs(count):
    log('VIEW ISSUE TABS')
    issues = issueList.getIssues()

    for i in range(count):
        issue = issues[i]
        cached = i > 0
        l.output('viewing issue ' + issue.getKey() + ' cached=' + str(cached))
        action = i % 4
        
        if action == 0:
            issue.viewTabAll(cached)
        elif action == 1:
            issue.viewTabActivityStream(cached)
        elif action == 2:
            issue.viewTabChangeHistory(cached)
        elif action == 3:
            issue.viewTabComments(cached)
    
def createIssues(count):
    log('CREATE ISSUES')
    token = issueList.getIssues()[0].view(True)['atl_token']
    
    for i in range(count):
        projectId = projectList.getRandomProject().getProjectId()
        l.output('creating issue for project ' + str(projectId))
        token = issueCreator.create(token, projectId, { 'summary' : 'test' + str(i), 'reporter' : user['username'] }, i > 0)
    
def editIssues(count):
    log('EDIT ISSUES')
    issues = issueList.getIssues()

    edited = 0
    for i in range(len(issues)-1):
        issue = issues[randint(0, len(issues)-1)]
        data = issue.view(True)
        if data['editable']:
            l.output('editing issue ' + issue.getKey())
            issue.edit(data['atl_token'], { 'description' : 'test ' + str(i) }, i>0)
            edited += 1
            if (edited >= count):
                break
    
def addComments(count):
    log('ADD COMMENTS')
    issues = issueList.getIssues()

    for i in range(count):
        issue = issues[randint(0, len(issues)-1)]
        l.output('commenting on issue ' + issue.getKey())
        token = issue.view(True)['atl_token']
        issue.addComment(token, { 'comment' : 'test ' + str(i) }, True)
    
def transitionIssues(count):
    log('WORKFLOW TRANSITION ON ISSUES')
    issues = issueList.getIssues()
    for index in range(count):
        issue = issues[index] 
        data = issue.view(True)
        actions = data['workflow_actions']
        nextStep = actions[randint(0, len(actions)-1)]
        token = data['atl_token']
        l.output('transitioning issue ' + issue.getKey() + ' to step ' + nextStep)
        issue.nextWorkflowStep(token, nextStep, {}, True)
    
def searchIssues(count):
    log('ISSUE SEARCH')
    issueNav.simpleSearch({ 'query' : '', 'summary' : 'true' })
    issueNav.simpleSearch({ 'query' : '', 'summary' : 'true' }, True)

    for i in range(count-2):
        issueNav.advancedSearch("PROJECT = " + projectList.getRandomProject().getProjectKey(), i > 0)
    
def useSavedFilter(count):
    log('USE A SAVED FILTER')
    for i in range(count):
        filterId = 10000 + randint(0, 4)
        l.output('viewing saved filter ' + str(filterId))
        cached = i>0
        token = savedFilter.manage(cached)
        savedFilter.use(token, filterId, cached)

def browseProjects(count):
    log('BROWSE PROJECT TABS')
    projects = projectList.getProjects()
    
    for i in range(count):
        project = projects[randint(0, len(projects)-1)]
        l.output('viewing project ' + project.getProjectKey())
        action = i % 3
        cached = i > 0
        
        if action == 0:
            project.viewTabComponents(cached)
        elif action == 1:
            project.viewTabIssues(cached)
        elif action == 2:
            project.viewTabVersions(cached)
    
def browseUserProfile():
    log('USER PROFILE')
    userProfile.view(user['username'])

def viewDashboard(count):
    log('VIEW DASHBOARD')
    for i in range(count):
        dashboard.loggedIn(True)

def viewReports(count):
    log('VIEW REPORTS')
    for i in range(count):
        projectId = projectList.getRandomProject().getProjectId()
        cached = i>0
        if i%2 == 0:
            reports.viewCreatedVsResolved(projectId, cached)
        else:
            reports.viewRecentlyCreatedIssues(projectId, cached)

# ========================== ADMIN ==========================

def doWebsudoLogin():
    log('WEBSUDO SESSION')
    data = userAdmin.browseSudo()
    websudo.sudo(data['token'], data['destination'], user['password'])

def createUser(count):
    log('CREATE USERS')
    for i in range(count):
        name = randomLowercase(8)
        l.output('creating user ' + name)
        cached = i>0
        token = userAdmin.browse(cached)
        userAdmin.add(token, { 'username' : name, 'password' : name, 'email' : name + '@example.com' }, cached)

def createProject(count):
    log('CREATE PROJECT')
    for i in range(count):
        key = randomUppercase(6)
        cached = i>0
        l.output('creating project with key [' + key + ']')
        
        projectAdmin.browse(cached)
        projectAdmin.add({ 'name' : key, 'key' : key, 'lead' : user['username'] }, cached)

def editProject(count):
    log('EDIT PROJECT')
    for i in range(count):
        project = projectList.getRandomProject()
        cached = i>0
        l.output('editing project [' + project.getProjectKey() + ']')
        token = projectAdmin.browse(cached)
        projectAdmin.edit(token, {'id' : project.getProjectId(), 'key' : project.getProjectKey(), 'name' : 'edited'}, cached)

def createIssueType(count):
    log('CREATE ISSUE TYPE')
    for i in range(count):
        name =  randomLowercase(8)
        cached = i>0
        l.output('creating issue type ' + name)
        token = issueTypeAdmin.browse(cached)
        issueTypeAdmin.add(token, { 'name' : name }, cached)

def editIssueType(count):
    log('EDIT ISSUE TYPE')
    for i in range(count):
        cached = i>0
        token = issueTypeAdmin.browse(cached)
        issueTypeAdmin.edit(token, { 'id' : '1', 'name' : 'edited' }, cached)

def createWorkflow(count):
    log('CREATE WORKFLOW')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        token = workflowAdmin.browse(cached)
        workflowAdmin.add(token, {'newWorkflowName' : name}, cached)

def editWorkflow(count):
    log('EDIT WORKFLOW')
    for i in range(count):
        cached = i>0
        token = workflowAdmin.browse(cached)
        workflowAdmin.edit(token, {'name' : 'Agile', 'description' : 'edited'}, cached)

def createScreen(count):
    log('CREATE SCREEN')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        token = screenAdmin.browse(cached)
        screenAdmin.add(token, { 'name' : name }, cached)

def editScreen(count):
    log('EDIT SCREEN')
    for i in range(count):
        cached = i>0
        token = screenAdmin.browse(cached)
        screenAdmin.edit(token, { 'id' : '1', 'description' : 'edited' }, cached)

def createNotificationScheme(count):
    log('CREATE NOTIFICATION SCHEME')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        token = notificationSchemeAdmin.browse(cached)
        notificationSchemeAdmin.add(token, { 'name' : name }, cached)

def editNotificationScheme(count):
    log('EDIT NOTIFICATION SCHEME')
    for i in range(count):
        cached = i>0
        token = notificationSchemeAdmin.browse(cached)
        notificationSchemeAdmin.edit(token, { 'id' : '10000', 'description' : 'edited' }, cached)

def createPriority(count):
    log('CREATE PRIORITY')
    for i in range(count):
        cached = i>0
        name = randomLowercase(8)
        token = priorityAdmin.browse(cached)
        priorityAdmin.add(token, { 'name' : name }, cached)
    
def editPriority(count):
    log('EDIT PRIORITY')
    for i in range(count):
        cached = i>0
        priorityAdmin.browse(cached)
        priorityAdmin.edit({ 'id' : '1', 'description' : 'edited' }, cached)

def createStatus(count):
    log('CREATE STATUS')
    for i in range(count):
        cached = i>0
        name = randomLowercase(8)
        token = statusAdmin.browse(cached)
        statusAdmin.add(token, { 'name' : name }, cached)
    
def editStatus(count):
    log('EDIT STATUS')
    for i in range(count):
        cached = i>0
        statusAdmin.browse(cached)
        statusAdmin.edit({ 'id' : '1', 'description' : 'edited' }, cached)
 
def createResolution(count):
    log('CREATE RESOLUTION')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        token = resolutionAdmin.browse(cached)
        resolutionAdmin.add(token, { 'name' : name }, cached)
    
def editResolution(count):
    log('EDIT RESOLUTION')
    for i in range(count):
        cached = i>0
        resolutionAdmin.browse(cached)
        resolutionAdmin.edit({ 'id' : '1', 'description' : 'edited' }, cached)
 
def createPermissionScheme(count):
    log('CREATE PERMISSION SCHEME')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        permissionSchemeAdmin.browse(cached)
        permissionSchemeAdmin.add({ 'name' : name }, cached)
    
def editPermissionScheme(count):
    log('EDIT PERMISSION SCHEME')
    for i in range(count):
        cached = i>0
        permissionSchemeAdmin.browse(count)
        permissionSchemeAdmin.edit({ 'id' : '0', 'description' : 'edited' }, cached)
 
def browseSystemInfo(count=1):
    log('BROWSE SYSTEM INFO')
    for i in range(count):
        systemInfoAdmin.browse(i>0)
 
def changeLogLevel(count=1):
    log('CHANGE LOG LEVEL')
    for i in range(count):
        level = randChoice(logLevels)
        cached = i>0
        token = loggingProfilingAdmin.browse(cached)
        l.output('changing log level of [com.atlassian.jira.util.BugzillaImportBean] to ' + level)
        # change something that potentially doesn't cause huge amounts of logging, as not to skew the tests
        loggingProfilingAdmin.changeLogLevel(token, 'com.atlassian.jira.util.BugzillaImportBean', level, cached)
 
def addMailServer(count=1):
    log('ADD MAIL SERVER')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        mailServerAdmin.browse(cached)
        mailServerAdmin.addSmtpServer({ 'name' : name, 'from' : 'test@atlassian.com', 'servername' : 'localhost' }, cached)
 
def changeGeneralConfiguration(count=1):
    log('CHANGE INTRODUCTION')
    for i in range(count):
        cached = i>0
        intro = randomLowercase(8)
        generalconfigadmin.browse(cached)
        generalconfigadmin.edit({ 'introduction' : intro, 'useGzip' : 'false' }, cached)
    
# ========================== UTIL ==========================

def log(message):
    l.output('============================= ' + message + ' =============================')

def randomUppercase(length):
    text = ''
    for i in range(length):
        text += randChoice(uppercase)
    return text

def randomLowercase(length):
    text = ''
    for i in range(length):
        text += randChoice(lowercase)
    return text
