from HTTPClient import NVPair 
from env import request, extract, valueOrDefault
from java.util.regex import Pattern

class GeneralConfigAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view general configuration'),
            'edit' : request(testId + 1, 'HTTP-REQ : edit general configuration')
        }
        self.patterns = {
            'edit_title' : Pattern.compile('(?s)name="title".*?value="(.*?)"'),
            'edit_mode' : Pattern.compile('(?s)id="mode_select".*value="(.*?)" SELECTED'),
            'edit_captcha' : Pattern.compile('value="(.*?)" name="captcha".*?checked'),
            'edit_baseURL' : Pattern.compile('(?s)name="baseURL".*?value="(.*?)"'),
            'edit_emailFromHeaderFormat' : Pattern.compile('(?s)name="emailFromHeaderFormat".*?value="(.*?)"'),
            'edit_introduction' : Pattern.compile('(?s)name="introduction".*?\>(.*?)\<'),
            'edit_encoding' : Pattern.compile('(?s)name="encoding".*?value="(.*?)"'),
            'edit_language' : Pattern.compile('(?s)name="language".*?value="(.*?)" SELECTED'),
            'edit_voting' : Pattern.compile('value="(.*?)" name="voting".*?checked'),
            'edit_watching' : Pattern.compile('value="(.*?)" name="watching".*?checked'),
            'edit_allowUnassigned' : Pattern.compile('value="(.*?)" name="allowUnassigned".*?checked'),
            'edit_externalUM' : Pattern.compile('value="(.*?)" name="externalUM".*?checked'),
            'edit_externalPM' : Pattern.compile('value="(.*?)" name="externalPM".*?checked'),
            'edit_logoutConfirm' : Pattern.compile('value="(.*?)".*?name="logoutConfirm" checked'),
            'edit_useGzip' : Pattern.compile('value="(.*?)" name="useGzip".*?checked'),
            'edit_allowRpc' : Pattern.compile('value="(.*?)" name="allowRpc".*?checked'),
            'edit_emailVisibility' : Pattern.compile('value="([a-z]*?)".*?name="emailVisibility" checked'),
            'edit_groupVisibility' : Pattern.compile('value="([a-z]*?)".*?name="groupVisibility" checked'),
            'edit_excludePrecedenceHeader' : Pattern.compile('value="(.*?)" name="excludePrecedenceHeader".*?checked'),
            'edit_ajaxIssuePicker' : Pattern.compile('value="(.*?)" name="ajaxIssuePicker".*?checked'),
            'edit_ajaxUserPicker' : Pattern.compile('value="(.*?)" name="ajaxUserPicker".*?checked'),
            'edit_jqlAutocompleteDisabled' : Pattern.compile('value="(.*?)" name="jqlAutocompleteDisabled".*?checked'),
            'edit_ieMimeSniffer' : Pattern.compile('(?s)name="ieMimeSniffer".*?value="([a-z]*?)"')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']
        req.GET('/secure/admin/jira/ViewApplicationProperties.jspa')
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/adminMenu.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/s/472/1/_/images/icons/navigate_right_10.gif')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/472/1/_/images/icons/help_blue.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
            
        
    def edit(self, config, cached=False):
        req = self.requests['edit']
        
        page = req.GET('/secure/admin/jira/EditApplicationProperties!default.jspa').text
        
        if not cached:
            req.GET('/s/472/1/0/_/styles/global.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.js')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:jira-global/jira.webresources:jira-global.css')
            req.GET('/s/472/1/1.0/_/download/batch/jira.webresources:header/jira.webresources:header.js')
            req.GET('/s/472/1/_/includes/js/adminMenu.js')
            req.GET('/s/472/1/_/styles/combined.css')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=3c78b5')
            req.GET('/s/472/1/_/images/icons/create_12.png')
            req.GET('/rest/api/1.0/header-separator?color=f0f0f0&bgcolor=3c78b5')
            req.GET('/rest/api/1.0/dropdowns?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ffffff&bgcolor=114070')
            req.GET('/rest/api/1.0/header-separator?color=ccc&bgcolor=f7f7f7')
            req.GET('/s/472/1/_/images/icons/navigate_down_10.gif')
            req.GET('/s/472/1/0/_/images/menu_indicator_for_light_backgrounds.gif')
            req.GET('/s/472/1/_/images/icons/navigate_right_10.gif')
            req.GET('/s/472/1/_/images/icons/help_blue.gif')
            req.GET('/images/border/spacer.gif')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rt.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/l.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/r.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/lb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/rb.png')
            req.GET('/s/472/1/1.2.4/_/download/batch/com.atlassian.auiplugin:ajs/images/shadow/b.png')
            req.GET('/s/472/1/_/images/jira111x30.png')
            req.GET('/s/472/1/_/styles/combined-printable.css')
                
                
                
        req.POST('/secure/admin/jira/EditApplicationProperties.jspa',
            (
                NVPair('title', valueOrDefault(config, 'title', extract(page, self.patterns['edit_title']))),
                NVPair('mode', valueOrDefault(config, 'mode', extract(page, self.patterns['edit_mode']))),
                NVPair('captcha', valueOrDefault(config, 'captcha', extract(page, self.patterns['edit_captcha']))),
                NVPair('baseURL', valueOrDefault(config, 'baseURL', extract(page, self.patterns['edit_baseURL']))),
                NVPair('emailFromHeaderFormat', valueOrDefault(config, 'emailFromHeaderFormat', extract(page, self.patterns['edit_emailFromHeaderFormat']))),
                NVPair('introduction', valueOrDefault(config, 'introduction', extract(page, self.patterns['edit_introduction']))),
                NVPair('encoding', valueOrDefault(config, 'encoding', extract(page, self.patterns['edit_encoding']))),
                NVPair('language', valueOrDefault(config, 'language', extract(page, self.patterns['edit_language']))),
                NVPair('defaultLocale', '-1'),
                NVPair('voting', valueOrDefault(config, 'voting', extract(page, self.patterns['edit_voting']))),
                NVPair('watching', valueOrDefault(config, 'watching', extract(page, self.patterns['edit_watching']))),
                NVPair('allowUnassigned', valueOrDefault(config, 'allowUnassigned', extract(page, self.patterns['edit_allowUnassigned']))),
                NVPair('externalUM', valueOrDefault(config, 'externalUM', extract(page, self.patterns['edit_externalUM']))),
                NVPair('externalPM', valueOrDefault(config, 'externalPM', extract(page, self.patterns['edit_externalPM']))),
                NVPair('logoutConfirm', valueOrDefault(config, 'logoutConfirm', extract(page, self.patterns['edit_logoutConfirm']))),
                NVPair('useGzip', valueOrDefault(config, 'useGzip', extract(page, self.patterns['edit_useGzip']))),
                NVPair('allowRpc', valueOrDefault(config, 'allowRpc', extract(page, self.patterns['edit_allowRpc']))),
                NVPair('emailVisibility', valueOrDefault(config, 'emailVisibility', extract(page, self.patterns['edit_emailVisibility']))),
                NVPair('groupVisibility', valueOrDefault(config, 'groupVisibility', extract(page, self.patterns['edit_groupVisibility']))),
                NVPair('excludePrecedenceHeader', valueOrDefault(config, 'excludePrecedenceHeader', extract(page, self.patterns['edit_excludePrecedenceHeader']))),
                NVPair('ajaxIssuePicker', valueOrDefault(config, 'ajaxIssuePicker', extract(page, self.patterns['edit_ajaxIssuePicker']))),
                NVPair('ajaxUserPicker', valueOrDefault(config, 'ajaxUserPicker', extract(page, self.patterns['edit_ajaxUserPicker']))),
                NVPair('jqlAutocompleteDisabled', valueOrDefault(config, 'jqlAutocompleteDisabled', extract(page, self.patterns['edit_jqlAutocompleteDisabled']))),
                NVPair('ieMimeSniffer', valueOrDefault(config, 'ieMimeSniffer', extract(page, self.patterns['edit_ieMimeSniffer']))),
                NVPair('Update', 'Update'),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))
        
        self.browse(cached)
        