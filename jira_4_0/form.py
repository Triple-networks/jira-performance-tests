from env import extract, extractAll
from java.util.regex import Pattern

formPatterns = {
    'action' : Pattern.compile('action="(.*?)"'),
    'select' : Pattern.compile('(?s)<select.*?name="(.*?)".*?>(.*?)<\/select>'),
    'options' : Pattern.compile('(?s)<option.*?value="(.*?)"'),
    'submit' : Pattern.compile('(?s)input.*?type="submit".*?name="(.*?)"'),
    'textfield' : Pattern.compile('(?s)input.*?type="text".*?name="(.*?)"'),
    'textarea' : Pattern.compile('(?s)textarea.*?name="(.*?)"')
}

def extractAction(form):
    return extract(form, formPatterns['action'])

def extractSelects(form):
    p = formPatterns['select']
    m = p.matcher(form)
    selects = []
    while(m.find()):
        selects.append( ( m.group(1), m.group(2) ), )
    return selects 

def extractOptions(select):
    p = formPatterns['options']
    m = p.matcher(select)
    options = []
    while(m.find()):
        options.append(m.group(1))
    return options

def extractSubmit(form):
    return extract(form, formPatterns['submit'])

def extractTextfields(form):
    return extractAll(form, formPatterns['textfield'])

def extractTextAreas(form):
    return extractAll(form, formPatterns['textarea'])

