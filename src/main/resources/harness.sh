#!/bin/bash

source common.sh

if [ -n "$2" ]; then
   case $2 in
      runadmin)   enable_admin
                  ;;
   esac
fi

# Start the remote grinders
echo_do_check "Starting remote grinder agents" \
    bg_wrapper launch_grinders $1 admin admin

# Waiting for the grinders to finish
echo_do_check "Waiting for grinders to finish" \
    wait 
