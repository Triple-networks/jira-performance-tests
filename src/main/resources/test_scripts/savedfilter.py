from env import request, cacheRequest, extract
from java.util.regex import Pattern

class SavedFilter:
    
    def __init__(self, testIndex):
        cacheIndex = testIndex * 100
        self.requests = {
            'manage' : request(testIndex, 'HTTP-REQ : manage saved filters'),
            'use' : request(testIndex + 1, 'HTTP-REQ : use a saved filter'),
            'filter_cache' : cacheRequest(cacheIndex, 'CACHE : filters'),
        }
        self.patterns = {
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"')                         
        }
        
    def manage(self, cached=False):
        req = self.requests['manage']
        cacheReq = self.requests['filter_cache']
        
        page = req.GET('/secure/ManageFilters.jspa').text
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:manageshared/jira.webresources:manageshared.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:managefilters/jira.webresources:managefilters.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/cog-dd.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/star_yellow.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            cacheReq.GET('/images/icons/filter_48.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
         
        return extract(page, self.patterns['atl_token'])           


    def use(self, atl_token, savedFilterId, cached=False):
        req = self.requests['use']
        cacheReq = self.requests['filter_cache']

        
        req.GET('/secure/IssueNavigator.jspa?mode=hide&atl_token=' + atl_token + '&requestId=' + str(savedFilterId))
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/css/jira.navigator.simple,atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/2b1ee4eaa4ba7880e509215b4ec88293/_/download/contextbatch/js/jira.navigator.simple,atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:searchcacheRequestview-charts/com.atlassian.jira.gadgets:searchcacheRequestview-charts.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js?context=issueaction&context=issuenavigation')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/view_20.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/tools_20.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/permalink_light_16.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_filtercollapse.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/tab_bg.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/star_yellow.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_descending.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_activeissue.png')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/images/icons/priority_major.gif')
            cacheReq.GET('/images/icons/status_open.gif')
            cacheReq.GET('/images/icons/status_reopened.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
